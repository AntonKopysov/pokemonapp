package com.anton.kop.pokemonApp.presentation;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SecurityTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private final String TEST_USERNAME = "test";
    private final String TEST_PASS = "test";

    @BeforeAll
    private void initUser() throws Exception {
        mockMvc.perform(post(String.format("/registration?username=%s&password=%s&isActive=true", TEST_USERNAME, TEST_PASS)));
    }

    @Test
    @Order(0)
    void noLoggedInRequestTest() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().is(403));
    }

    @Test
    @Order(1)
    void registrationTest() throws Exception {
        mockMvc.perform
                (post(String.format("/registration?username=%s&password=%s&isActive=true", TEST_USERNAME, passwordEncoder.encode(TEST_PASS))))
                .andExpect(status().is(302));
    }

    @Test
    @Order(2)
    void loginTest() throws Exception {
        mockMvc.perform(post(String.format("/login?username=%s&password=%s", TEST_USERNAME, TEST_PASS)))
                .andExpect(status().is3xxRedirection());
        int itemsCount = 10;
        mockMvc.perform(get("/pokemons/?page=1&limit=" + itemsCount))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reducedPokemonEntities").isArray())
                .andExpect(jsonPath("$.reducedPokemonEntities", Matchers.hasSize(itemsCount)))
                .andExpect(jsonPath("$.reducedPokemonEntities.[?(@.name == '%s')]", "squirtle").exists());
    }

    @Test
    @Order(3)
    void logoutTest() throws Exception {
        mockMvc.perform(get("/logout"))
                .andExpect(status().is3xxRedirection());
        mockMvc.perform(get("/"))
                .andExpect(status().is(403));
    }
}
