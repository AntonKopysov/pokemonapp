package com.anton.kop.pokemonApp.presentation;

import com.anton.kop.pokemonApp.data.client.model.Services;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest
class RESTControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @ParameterizedTest
    @WithMockUser(username = "spring")
    @EnumSource(value = Services.ServicesKeys.class, mode = EnumSource.Mode.MATCH_ANY,
            names = {"ABILITY", "BERRY", "BERRY_FIRMNESS", "BERRY_FLAVOR", "CHARACTERISTIC", "CONTEST_EFFECT",
                    "CONTEST_TYPE", "EGG_GROUP", "ENCOUNTER_CONDITION", "ENCOUNTER_CONDITION_VALUE", "ENCOUNTER_METHOD",
                    "EVOLUTION_CHAIN", "EVOLUTION_TRIGGER", "GENDER", "GENERATION", "GROWTH_RATE", "ITEM", "ITEM_ATTRIBUTE",
                    "ITEM_CATEGORY", "ITEM_FLING_EFFECT", "ITEM_POCKET", "LANGUAGE", "LOCATION", "LOCATION_AREA", "MACHINE",
                    "MOVE", "MOVE_AILMENT", "MOVE_BATTLE_STYLE", "MOVE_CATEGORY", "MOVE_DAMAGE_CLASS", "MOVE_LEARN_METHOD",
                    "MOVE_TARGET", "NATURE", "PAL_PARK_AREA", "POKEATHLON_STAT", "POKEDEX", "POKEMON", "POKEMON_COLOR",
                    "POKEMON_FORM", "POKEMON_HABITAT", "POKEMON_SHAPE", "POKEMON_SPECIES", "REGION", "STAT",
                    "SUPER_CONTEST_EFFECT", "TYPE", "VERSION", "VERSION_GROUP"})
    void testGetServices(Services.ServicesKeys keys) throws Exception {
        this.mockMvc.perform(get("/"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.serviceEntities").isArray())
                .andExpect(jsonPath("$.serviceEntities", Matchers.hasSize(48)))
                .andExpect(jsonPath("$.serviceEntities.[0]").isMap())
                .andExpect(jsonPath("$.serviceEntities.[?(@.name == '%s')]", keys).exists());
    }

    @Test
    @WithMockUser(username = "spring")
    void testGetPokemon() throws Exception {
        this.mockMvc.perform(get("/pokemon/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$", Matchers.hasEntry("name", "bulbasaur")))
                .andExpect(jsonPath("$", Matchers.hasEntry("id", 1)));
    }

    @Test
    @WithMockUser(username = "spring")
    void testGetPokemons() throws Exception {
        int itemsCount = 10;
        this.mockMvc.perform(get("/pokemons/?page=1&limit=" + itemsCount))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reducedPokemonEntities").isArray())
                .andExpect(jsonPath("$.reducedPokemonEntities", Matchers.hasSize(itemsCount)))
                .andExpect(jsonPath("$.reducedPokemonEntities.[?(@.name == '%s')]", "squirtle").exists());
    }

    @Test
    @WithMockUser(username = "spring")
    public void testGetPokemonsBadRequest() throws Exception {
        this.mockMvc.perform(get("/pokemons/?page=fdsg&limit=fsg"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(username = "spring")
    void testGetPokemonsNestedServletException() {
        assertThrows(NestedServletException.class, () -> this.mockMvc.perform(get("/pokemons/?page=-5&limit=-5")));
    }

    @Test
    @WithMockUser(username = "spring")
    void testGetPokemonByName() throws Exception {
        this.mockMvc.perform(get("/pokemon/?name=zapdos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pokemonEntity").isNotEmpty())
                .andExpect(jsonPath("$.pokemonEntity").isMap())
                .andExpect(jsonPath("$.pokemonEntity", Matchers.hasEntry("name", "zapdos")))
                .andExpect(jsonPath("$.pokemonEntity", Matchers.hasEntry("id", 145)));
    }
}
