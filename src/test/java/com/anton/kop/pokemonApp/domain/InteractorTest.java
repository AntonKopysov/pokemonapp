package com.anton.kop.pokemonApp.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class InteractorTest {

    @InjectMocks
    private Interactor domainService;
    @Mock
    private IServicesDataSource servicesDataSource;
    @Mock
    private IPokemonsListDataSource pokemonsListDataSource;
    @Mock
    private IPokemonDataSource pokemonDataSource;

    @BeforeEach
    private void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getServices() {
        domainService.getServices();
        verify(servicesDataSource, times(1)).getAllServiceEntities();
    }

    @Test
    void startService() {
        domainService.startService();
        verify(servicesDataSource, times(1)).saveAllServiceEntitiesToDb();
        verify(pokemonsListDataSource, times(1)).getAllReducedPokemonEntitys();
    }
}