package com.anton.kop.pokemonApp;

import com.anton.kop.pokemonApp.data.PokemonMapper;
import com.anton.kop.pokemonApp.data.client.model.NameAndApiUrl;
import com.anton.kop.pokemonApp.data.client.model.pokemon.Pokemon;
import com.anton.kop.pokemonApp.data.client.model.pokemon.PokemonAbility;
import com.anton.kop.pokemonApp.data.client.model.pokemon.PokemonStat;
import com.anton.kop.pokemonApp.data.client.model.pokemon.Stats;
import com.anton.kop.pokemonApp.data.dto.AbilitiesDTO;
import com.anton.kop.pokemonApp.data.dto.PokemonDTO;
import com.anton.kop.pokemonApp.data.dto.ReducedPokemonDTO;
import com.anton.kop.pokemonApp.domain.entities.PokemonEntity;
import com.anton.kop.pokemonApp.domain.entities.ReducedPokemonEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class PokemonAppApplicationTests {

	@Autowired
	private PokemonMapper pokemonMapper;

	@Test
	void contextLoads() {
	}

	@Test
	void testPokemonDToToEntity() {
		PokemonDTO pokemonDTO = TestHelper.getRandomPokemonDTO(1);
		PokemonEntity pokemonEntity = pokemonMapper.pokemonDToToEntity(pokemonDTO);
		assert (pokemonEntity.getName().equals(pokemonDTO.getName()));
		assert (pokemonEntity.getId().equals(pokemonDTO.getId()));
		assert (pokemonEntity.getHeight().equals(pokemonDTO.getHeight()));
		assert (pokemonEntity.getWeight().equals(pokemonDTO.getWeight()));
		assert (pokemonEntity.getAbilities().get(0).getName().equals(pokemonDTO.getAbilities().get(0).getName()));
		assert (pokemonEntity.getAbilities().get(0).getUrl().equals(pokemonDTO.getAbilities().get(0).getUrl()));
		assert (pokemonEntity.getStats().get(0).getStat().equals(pokemonDTO.getStats().get(0).getStat()));
	}

	@Test
	void testPokemonToPokemonDTO() {
		Pokemon pokemon = new Pokemon();
		pokemon.setName("name");
		pokemon.setId(1);
        PokemonStat pokemonStat = new PokemonStat();
        pokemonStat.setStat(Stats.SPECIAL_ATTACK);
        pokemonStat.setEffort(4);
        pokemonStat.setBaseStat(1);
        pokemon.setStats(List.of(pokemonStat));
		PokemonDTO pokemonDTO = pokemonMapper.pokemonToPokemonDTO(pokemon);
		assert (pokemonDTO.getName().equals(pokemon.getName()));
		assert (pokemonDTO.getId().equals(pokemon.getId()));
		assert (pokemonDTO.getStats().get(0).getStat().getUrl().equals(pokemon.getStats().get(0).getStat().getUrl()));
		assert (pokemonDTO.getStats().get(0).getStat().getName().equals(pokemon.getStats().get(0).getStat().getName()));
		assert (pokemonDTO.getStats().get(0).getStat().getName().equals(pokemon.getStats().get(0).getStat().getName()));
		assert (pokemonDTO.getStats().get(0).getBaseStat().equals(pokemon.getStats().get(0).getBaseStat()));
	}

	@Test
	void testAbilitiesToAbilitiesDTO() {
		NameAndApiUrl ability = new NameAndApiUrl();
		ability.setName("name");
		ability.setUrl("url");
        PokemonAbility pokemonAbility = new PokemonAbility();
        pokemonAbility.setSlot(3);
        pokemonAbility.setHidden(false);
        pokemonAbility.setAbility(ability);
		AbilitiesDTO dto = pokemonMapper.abilitiesToAbilitiesDTO(pokemonAbility, null);
		assert (dto.getUrl().equals(pokemonAbility.getAbility().getUrl()));
		assert (dto.getName().equals(pokemonAbility.getAbility().getName()));
		assert (dto.getSlot().equals(pokemonAbility.getSlot()));
		assert (dto.isHidden() == (pokemonAbility.isHidden()));
	}

	@Test
	void testPokemonItemDToToListEntity() {
		ReducedPokemonDTO reducedPokemonDTO = new ReducedPokemonDTO(1, "name", "https://habr.com/ru/post/464881/");
		ReducedPokemonEntity reducedPokemonEntity = pokemonMapper.reducedPokemonDToToReducedPokemonEntity(reducedPokemonDTO);
		assert (reducedPokemonEntity.getId().equals(reducedPokemonDTO.getId()));
		assert (reducedPokemonEntity.getName().equals(reducedPokemonDTO.getName()));
		assert (reducedPokemonEntity.getUrl().equals(reducedPokemonDTO.getUrl()));
	}
}
