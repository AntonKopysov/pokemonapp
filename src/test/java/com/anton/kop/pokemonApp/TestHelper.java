package com.anton.kop.pokemonApp;

import com.anton.kop.pokemonApp.data.client.model.pokemon.Stats;
import com.anton.kop.pokemonApp.data.dto.AbilitiesDTO;
import com.anton.kop.pokemonApp.data.dto.PokemonDTO;
import com.anton.kop.pokemonApp.data.dto.StatDTO;

import java.util.List;
import java.util.Random;
import java.util.UUID;

public class TestHelper {

    Random random = new Random();

    public static PokemonDTO getRandomPokemonDTO(int id) {
        Random random = new Random();
        AbilitiesDTO abilitiesDTO = new AbilitiesDTO();
        abilitiesDTO.setHidden(false);
        abilitiesDTO.setSlot(random.nextInt());
        abilitiesDTO.setUrl(UUID.randomUUID().toString());
        abilitiesDTO.setName(UUID.randomUUID().toString());
        StatDTO statDTO = new StatDTO();
        statDTO.setStat(Stats.ACCURACY);
        return new PokemonDTO(id, random.nextInt(), random.nextInt(), random.nextInt(), UUID.randomUUID().toString(), List.of(abilitiesDTO), List.of(statDTO));
    }


}
