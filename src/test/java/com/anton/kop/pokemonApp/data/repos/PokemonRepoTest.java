package com.anton.kop.pokemonApp.data.repos;

import com.anton.kop.pokemonApp.TestHelper;
import com.anton.kop.pokemonApp.data.dto.PokemonDTO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@DataJpaTest
class PokemonRepoTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private IPokemonRepo pokemonRepo;
    private final int ID = 7;
    private PokemonDTO pokemonDTO;


    @BeforeAll
    private void initPokemon() {
        pokemonDTO = TestHelper.getRandomPokemonDTO(ID);
    }

    @Test
    void testGetPokemonById() {
        entityManager.persist(pokemonDTO);
        Optional<PokemonDTO> result = pokemonRepo.findById(ID);
        assertTrue(result.isPresent());
        assertEquals(pokemonDTO.getName(), result.get().getName());
        assertEquals(pokemonDTO.getExperience(), result.get().getExperience());
        assertEquals(pokemonDTO.getHeight(), result.get().getHeight());
        assertEquals(pokemonDTO.getWeight(), result.get().getWeight());
        assertEquals(pokemonDTO.getAbilities(), result.get().getAbilities());
        assertEquals(pokemonDTO.getStats(), result.get().getStats());
    }

    @Test
    void testPokemonNotExist() {
        entityManager.persist(pokemonDTO);
        Optional<PokemonDTO> result = pokemonRepo.findById(ID + 3);
        assertFalse(result.isPresent());
        assertThrows(NoSuchElementException.class, result::get);
    }

}