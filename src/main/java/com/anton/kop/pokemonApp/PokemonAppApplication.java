package com.anton.kop.pokemonApp;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class PokemonAppApplication {

	public static void main(String[] args) {
		log.debug("start app");
		SpringApplication.run(PokemonAppApplication.class, args);
	}
}
