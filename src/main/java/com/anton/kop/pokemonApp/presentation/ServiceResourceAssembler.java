package com.anton.kop.pokemonApp.presentation;

import com.anton.kop.pokemonApp.domain.entities.ServiceEntity;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ServiceResourceAssembler implements ResourceAssembler<Collection<ServiceEntity>, ServicesResource> {
    @Override
    public ServicesResource toResource(Collection<ServiceEntity> serviceEntities) {
        ServicesResource result = new ServicesResource(serviceEntities);
        result.add(getLinks(serviceEntities));
        return result;
    }

    private List<Link> getLinks(Collection<ServiceEntity> serviceEntities) {
        return List.of(
                linkTo(methodOn(AppRestController.class).getServices())
                        .withSelfRel()
                        .withTitle("Get Services")
                        .withType("GET")
        );
    }
}
