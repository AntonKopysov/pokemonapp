package com.anton.kop.pokemonApp.presentation;

import com.anton.kop.pokemonApp.domain.entities.PokemonEntity;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class PokemonItemResourceAssembler implements ResourceAssembler<PokemonEntity, PokemonItemResource> {

    @Override
    public PokemonItemResource toResource(PokemonEntity pokemonEntity) {
        return new PokemonItemResource(pokemonEntity);
    }
}
