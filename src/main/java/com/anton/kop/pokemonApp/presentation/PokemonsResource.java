package com.anton.kop.pokemonApp.presentation;

import com.anton.kop.pokemonApp.domain.entities.ReducedPokemonEntity;
import lombok.Value;
import org.springframework.hateoas.ResourceSupport;

import java.util.Collection;

@Value
public class PokemonsResource extends ResourceSupport {
    Collection<ReducedPokemonEntity> reducedPokemonEntities;
}
