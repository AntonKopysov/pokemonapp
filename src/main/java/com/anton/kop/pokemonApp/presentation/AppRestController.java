package com.anton.kop.pokemonApp.presentation;

import com.anton.kop.pokemonApp.domain.IDomainService;
import com.anton.kop.pokemonApp.domain.entities.PokemonEntity;
import com.anton.kop.pokemonApp.domain.entities.ReducedPokemonEntity;
import com.anton.kop.pokemonApp.domain.entities.ServiceEntity;
import com.anton.kop.pokemonApp.domain.entities.User;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Slf4j
@RequiredArgsConstructor
@CrossOrigin
@RestController
public class AppRestController {

    private final IDomainService service;
    private final ServiceResourceAssembler assembler;
    private final PokemonsResourceAssembler pokemonsResourceAssembler;
    private final PokemonItemResourceAssembler pokemonItemResourceAssembler;
    private final UserResourceAssembler userResourceAssembler;

    @GetMapping("/")
    @ApiOperation(value = "get list of all services")
    public HttpEntity<ServicesResource> getServices() {
        Collection<ServiceEntity> serviceEntities = service.getServices();
        log.debug("get {} serviceEntities", serviceEntities.size());
        ServicesResource servicesResource = assembler.toResource(serviceEntities);
        return ResponseEntity.ok(servicesResource);
    }

    @PostMapping(value = "/login", params = {"username", "password"})
    void login(@RequestParam("username") String username, @RequestParam("password") String password) {
    }

    @PostMapping(value = "/registration")
    @ApiOperation(value = "register new user")
    HttpEntity<UserResource> registration(User user) {
        User userFromDb = service.getUserByName(user.getUsername());
        if (userFromDb != null) {
            throw new UserAlreadyExist(String.format("User with username: %s already exist!", userFromDb.getUsername()));
        }
        UserResource resource = userResourceAssembler.toResource(user);
        service.saveUser(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(resource);
    }

    @GetMapping("/pokemon/{id}")
    @ApiOperation(value = "get pokemon by id")
    HttpEntity<PokemonEntity> getPokemon(@PathVariable Integer id) {
        return ResponseEntity.ok(service.getPokemon(id));
    }

    @GetMapping(value = "/pokemons/", params = {"page", "limit"})
    @ApiOperation(value = "get list of pokemons")
    HttpEntity<PokemonsResource> getPokemons(@RequestParam("page") Integer page, @RequestParam("limit") Integer limit) {
        Collection<ReducedPokemonEntity> pokemonsListEntities = service.getPokemonsList(page, limit);
        PokemonsResource pokemonsResource = pokemonsResourceAssembler.getPagedResource(pokemonsListEntities, page, limit);
        return ResponseEntity.ok(pokemonsResource);
    }

    @GetMapping(value = "/pokemon/", params = {"name"})
    @ApiOperation(value = "get pokemon by name")
    public HttpEntity<PokemonItemResource> getPokemonByName(@RequestParam("name") String name) {
        PokemonEntity pokemonsListEntity = service.getPokemonByName(name);
        PokemonItemResource itemResource = pokemonItemResourceAssembler.toResource(pokemonsListEntity);
        return ResponseEntity.ok(itemResource);
    }
}
