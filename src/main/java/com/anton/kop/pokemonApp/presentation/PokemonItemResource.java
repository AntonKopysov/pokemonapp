package com.anton.kop.pokemonApp.presentation;

import com.anton.kop.pokemonApp.domain.entities.PokemonEntity;
import lombok.Value;
import org.springframework.hateoas.ResourceSupport;

@Value
public class PokemonItemResource extends ResourceSupport {
    PokemonEntity pokemonEntity;
}
