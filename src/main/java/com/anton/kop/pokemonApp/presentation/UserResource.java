package com.anton.kop.pokemonApp.presentation;

import com.anton.kop.pokemonApp.domain.entities.User;
import lombok.Value;
import org.springframework.hateoas.ResourceSupport;

@Value
public class UserResource extends ResourceSupport {
    User user;
}
