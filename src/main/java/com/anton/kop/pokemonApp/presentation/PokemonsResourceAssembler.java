package com.anton.kop.pokemonApp.presentation;

import com.anton.kop.pokemonApp.domain.entities.ReducedPokemonEntity;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class PokemonsResourceAssembler implements ResourceAssembler<Collection<ReducedPokemonEntity>, PokemonsResource> {

    @Override
    public PokemonsResource toResource(Collection<ReducedPokemonEntity> entities) {
        //page 1 and limit 10 it's default values, whatever this method newer used
        return getPagedResource(entities, 1, 10);
    }

    public PokemonsResource getPagedResource(Collection<ReducedPokemonEntity> entities, int page, int limit) {
        for (ReducedPokemonEntity entity : entities) {
            changeUrl(entity);
        }
        PokemonsResource result = new PokemonsResource(entities);
        result.add(getLinks(entities, page, limit));
        return result;
    }

    /**
     * method change url from pokeapi.co url's to url's of this poxy service
     */
    ReducedPokemonEntity changeUrl(ReducedPokemonEntity reducedPokemonEntity) {
        reducedPokemonEntity.setUrl(linkTo(methodOn(AppRestController.class).getPokemon(reducedPokemonEntity.getId())).toString());
        return reducedPokemonEntity;
    }

    private List<Link> getLinks(Collection<ReducedPokemonEntity> entities, int page, int limit) {
        if (entities.size() < limit) {
            return List.of(linkTo(methodOn(AppRestController.class).getPokemons(page - 1, limit))
                    .withSelfRel()
                    .withTitle("Previous")
                    .withType("GET"));
        }
        if (page > 1) {
            return List.of(
                    linkTo(methodOn(AppRestController.class).getPokemons(page - 1, limit))
                            .withSelfRel()
                            .withTitle("Previous")
                            .withType("GET"),
                    linkTo(methodOn(AppRestController.class).getPokemons(page + 1, limit))
                            .withSelfRel()
                            .withTitle("Next")
                            .withType("GET")
            );
        } else if (page == 1) {
            return List.of(linkTo(methodOn(AppRestController.class).getPokemons(page + 1, limit))
                    .withSelfRel()
                    .withTitle("Next")
                    .withType("GET"));
        } else throw new IllegalArgumentException("page must be > 0");
    }
}
