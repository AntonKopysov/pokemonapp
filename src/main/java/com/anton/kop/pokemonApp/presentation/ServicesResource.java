package com.anton.kop.pokemonApp.presentation;

import com.anton.kop.pokemonApp.domain.entities.ServiceEntity;
import lombok.Value;
import org.springframework.hateoas.ResourceSupport;

import java.util.Collection;

@Value
class ServicesResource extends ResourceSupport {
    Collection<ServiceEntity> serviceEntities;
}
