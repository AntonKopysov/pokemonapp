package com.anton.kop.pokemonApp.presentation;

import com.anton.kop.pokemonApp.domain.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserResourceAssembler implements ResourceAssembler<User, UserResource> {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserResource toResource(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return new UserResource(user);
    }
}
