package com.anton.kop.pokemonApp.data.client.model.pokemon;

import com.anton.kop.pokemonApp.data.client.model.NameAndApiUrl;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PokemonGameIndex {
    @JsonProperty("game_index")
    private Integer gameIndex;
    private NameAndApiUrl version;
}
