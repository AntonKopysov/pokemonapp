package com.anton.kop.pokemonApp.data;

import com.anton.kop.pokemonApp.data.dto.UserDTO;
import com.anton.kop.pokemonApp.domain.entities.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User userDToToUser(UserDTO userDTO);

    UserDTO userToUserDTO(User user);
}
