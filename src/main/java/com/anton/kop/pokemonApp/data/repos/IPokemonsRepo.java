package com.anton.kop.pokemonApp.data.repos;

import com.anton.kop.pokemonApp.data.dto.ReducedPokemonDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPokemonsRepo extends JpaRepository<ReducedPokemonDTO, Integer> {

}
