package com.anton.kop.pokemonApp.data.client.model;

import lombok.Data;

import java.util.List;

/**
 * base class for requests like  this: :
 * "count": *,
 * "next": *,
 * "previous": *,
 * "results": [
 * {
 * *****
 * },...]
 *
 * @param <T> class that contained in "results": []
 */

@Data
public abstract class AbstractServiceResults<T>  {

    Integer count;
    String next;
    String previous;
    List<T> results;
}
