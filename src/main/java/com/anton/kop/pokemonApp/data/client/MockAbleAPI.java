package com.anton.kop.pokemonApp.data.client;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MockAbleAPI {
}
