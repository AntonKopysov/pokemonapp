package com.anton.kop.pokemonApp.data.client;

import com.anton.kop.pokemonApp.data.client.model.PokemonService;
import com.anton.kop.pokemonApp.data.client.model.Services;
import com.anton.kop.pokemonApp.data.client.model.pokemon.Pokemon;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.EnumMap;

/**
 * class to make requests to https://pokeapi.co/api/v2/  and deserialization received data
 */

@Slf4j
@MockAbleAPI
@Component
public class PokemonApiCall implements IPokemonApi {
    private RestTemplate restTemplate;
    @Value("${api.url}")
    private String apiUrl;

    public PokemonApiCall(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public EnumMap<Services.ServicesKeys, String> getServicesCall() {
        log.debug("make getServicesCall()");
        String response = restTemplate.getForObject(apiUrl, String.class);
        EnumMap<Services.ServicesKeys, String> map = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            map = mapper.readValue(response, new TypeReference<EnumMap<Services.ServicesKeys,String>>(){});
            log.debug("get and deserialize {} objects in response", map.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }

    @Override
    public PokemonService getPokemonsCall(@NonNull Integer offset, @NonNull Integer limit) {
        log.debug("make getServicesCall(), offset{}, limit{}", offset, limit);
        PokemonService pokemonService = null;
        try {
            pokemonService = restTemplate.getForObject
                    (apiUrl + String.format("pokemon/?offset=%d&limit=%d", offset, limit), PokemonService.class);
            log.debug("get pokemon services, count:{}", pokemonService.getCount());
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        return pokemonService;
    }

    @Override
    public Pokemon getPokemonCall(@NonNull Integer id) {
        log.debug("make getPokemonCall, id: {}", id);
        Pokemon pokemon = null;
        try {
            pokemon = restTemplate.getForObject(apiUrl + "pokemon/" + id, Pokemon.class);
            log.debug("get pokemon, id: {}, name: {}", pokemon.getId(), pokemon.getName());
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        return pokemon;
    }
}
