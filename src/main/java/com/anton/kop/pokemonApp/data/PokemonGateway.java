package com.anton.kop.pokemonApp.data;

import com.anton.kop.pokemonApp.data.client.IPokemonApi;
import com.anton.kop.pokemonApp.data.client.model.pokemon.Pokemon;
import com.anton.kop.pokemonApp.data.client.model.pokemon.PokemonAbility;
import com.anton.kop.pokemonApp.data.dto.AbilitiesDTO;
import com.anton.kop.pokemonApp.data.dto.PokemonDTO;
import com.anton.kop.pokemonApp.data.dto.StatDTO;
import com.anton.kop.pokemonApp.data.repos.IAbilitiesRepo;
import com.anton.kop.pokemonApp.data.repos.IPokemonRepo;
import com.anton.kop.pokemonApp.data.repos.IStatsRepo;
import com.anton.kop.pokemonApp.domain.IPokemonDataSource;
import com.anton.kop.pokemonApp.domain.entities.PokemonEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@Component
public class PokemonGateway implements IPokemonDataSource {

    private IPokemonRepo pokemonRepo;
    private PokemonMapper pokemonMapper;
    private IPokemonApi pokemonApi;
    private IAbilitiesRepo abilitiesRepo;
    private IStatsRepo statsRepo;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PokemonEntity getPokemonEntityById(Integer id) {
        return pokemonRepo.findById(id)
                .map(pokemonDTO -> pokemonMapper.pokemonDToToEntity(pokemonDTO))
                .orElseGet(() ->
                        {
                            Pokemon pokemon = pokemonApi.getPokemonCall(id);
                            PokemonDTO pokemonDTO = pokemonMapper.pokemonToPokemonDTO(pokemon);
                            pokemonDTO.setAbilities(convertAbilities(pokemon.getAbilities(), pokemonDTO));
                            for (StatDTO stat : pokemonDTO.getStats()) {
                                statsRepo.save(stat);
                                stat.setPokemon(Set.of(pokemonDTO));
                            }
                            pokemonRepo.save(pokemonDTO);
                            return pokemonMapper.pokemonToPokemonEntity(pokemon);
                        }
                );
    }

    private List<AbilitiesDTO> convertAbilities(Collection<PokemonAbility> abilities, PokemonDTO pokemonDTO) {
        ArrayList<AbilitiesDTO> result = new ArrayList<>();
        abilities.stream().map(ability -> pokemonMapper.abilitiesToAbilitiesDTO(ability, Set.of(pokemonDTO))).forEach(dto -> {
            abilitiesRepo.save(dto);
            result.add(dto);
        });
        return result;
    }
}
