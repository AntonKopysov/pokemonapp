package com.anton.kop.pokemonApp.data.client.model.pokemon;

import com.anton.kop.pokemonApp.data.client.model.NameAndApiUrl;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.EnumMap;
import java.util.List;

/**
 * class for deserialization response from https://pokeapi.co/api/v2/pokemon/{pokemon id}
 */

@Data
public class Pokemon {

    private List<PokemonAbility> abilities;
    @JsonProperty("base_experience")
    private Integer baseExperience;
    private List<NameAndApiUrl> forms;
    @JsonProperty("game_indices")
    private List<PokemonGameIndex> gameIndices;
    private Integer height;
    @JsonProperty("held_items")
    private List<PokemonHeldItems> heldItems;
    private Integer id;
    @JsonProperty("is_default")
    private boolean isDefault;
    @JsonProperty("location_area_encounters")
    private String locationAreaEncounters;
    private List<PokemonMove> moves;
    private String name;
    private Integer order;
    private NameAndApiUrl species;
    private EnumMap<PokemonSpritesKey, String> sprites;
    private List<PokemonStat> stats;
    private List<PokemonType> types;
    private Integer weight;
}
