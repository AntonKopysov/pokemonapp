package com.anton.kop.pokemonApp.data.client.model.pokemon;

import com.anton.kop.pokemonApp.data.client.model.NameAndApiUrl;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class VersionGroupDetail {

    @JsonProperty("level_learned_at")
    int levelLearnedAt;
    @JsonProperty("move_learn_method")
    NameAndApiUrl moveLearnMethod;
    @JsonProperty("version_group")
    NameAndApiUrl versionGroup;
}
