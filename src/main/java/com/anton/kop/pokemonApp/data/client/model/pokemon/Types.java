package com.anton.kop.pokemonApp.data.client.model.pokemon;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum Types {

    NORMAL("normal", "https://pokeapi.co/api/v2/type/1/"),
    FIGHTING("fighting", "https://pokeapi.co/api/v2/type/2/"),
    FLYING("flying", "https://pokeapi.co/api/v2/type/3/"),
    POISON("poison", "https://pokeapi.co/api/v2/type/4/"),
    GROUND("ground", "https://pokeapi.co/api/v2/type/5/"),
    ROCK("rock", "https://pokeapi.co/api/v2/type/6/"),
    BUG("bug", "https://pokeapi.co/api/v2/type/7/"),
    GHOST("ghost", "https://pokeapi.co/api/v2/type/8/"),
    STEEL("steel", "https://pokeapi.co/api/v2/type/9/"),
    FIRE("fire", "https://pokeapi.co/api/v2/type/10/"),
    WATER("water", "https://pokeapi.co/api/v2/type/11/"),
    GRASS("grass", "https://pokeapi.co/api/v2/type/12/"),
    ELECTRIC("electric", "https://pokeapi.co/api/v2/type/13/"),
    PSYCHIC("psychic", "https://pokeapi.co/api/v2/type/14/"),
    ICE("ice", "https://pokeapi.co/api/v2/type/15/"),
    DRAGON("dragon", "https://pokeapi.co/api/v2/type/16/"),
    DARK("dark", "https://pokeapi.co/api/v2/type/17/"),
    FAIRY("fairy", "https://pokeapi.co/api/v2/type/18/"),
    UNKNOWN("unknown", "https://pokeapi.co/api/v2/type/10001/"),
    SHADOW("shadow", "https://pokeapi.co/api/v2/type/10002/");

    @Getter
    @JsonProperty("name")
    private String name;
    @Getter
    private String url;

    Types(String name, String url) {
        this.name = name;
        this.url = url;
    }

    @JsonCreator
    public static Types fromName(final JsonNode jsonNode) {
        for (Types type : Types.values()) {
            if (type.name.equals(jsonNode.get("name").asText())) {
                return type;
            }
        }
        log.error("enum element with name {} not found", jsonNode.get("name").asText());
        return null;
    }
}
