package com.anton.kop.pokemonApp.data.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.EnumMap;

/**
 * class for deserialization response from https://pokeapi.co/api/v2/
 *  services names contain at enum ServicesKeys
 */

public class Services {

    @Getter
    @Setter
    private EnumMap<ServicesKeys, String> servicesResponse;

    public enum ServicesKeys {
        @JsonProperty("ability")
        ABILITY,
        @JsonProperty("berry")
        BERRY,
        @JsonProperty("berry-firmness")
        BERRY_FIRMNESS,
        @JsonProperty("berry-flavor")
        BERRY_FLAVOR,
        @JsonProperty("characteristic")
        CHARACTERISTIC,
        @JsonProperty("contest-effect")
        CONTEST_EFFECT,
        @JsonProperty("contest-type")
        CONTEST_TYPE,
        @JsonProperty("egg-group")
        EGG_GROUP,
        @JsonProperty("encounter-condition")
        ENCOUNTER_CONDITION,
        @JsonProperty("encounter-condition-value")
        ENCOUNTER_CONDITION_VALUE,
        @JsonProperty("encounter-method")
        ENCOUNTER_METHOD,
        @JsonProperty("evolution-chain")
        EVOLUTION_CHAIN,
        @JsonProperty("evolution-trigger")
        EVOLUTION_TRIGGER,
        @JsonProperty("gender")
        GENDER,
        @JsonProperty("generation")
        GENERATION,
        @JsonProperty("growth-rate")
        GROWTH_RATE,
        @JsonProperty("item")
        ITEM,
        @JsonProperty("item-attribute")
        ITEM_ATTRIBUTE,
        @JsonProperty("item-category")
        ITEM_CATEGORY,
        @JsonProperty("item-fling-effect")
        ITEM_FLING_EFFECT,
        @JsonProperty("item-pocket")
        ITEM_POCKET,
        @JsonProperty("language")
        LANGUAGE,
        @JsonProperty("location")
        LOCATION,
        @JsonProperty("location-area")
        LOCATION_AREA,
        @JsonProperty("machine")
        MACHINE,
        @JsonProperty("move")
        MOVE,
        @JsonProperty("move-ailment")
        MOVE_AILMENT,
        @JsonProperty("move-battle-style")
        MOVE_BATTLE_STYLE,
        @JsonProperty("move-category")
        MOVE_CATEGORY,
        @JsonProperty("move-damage-class")
        MOVE_DAMAGE_CLASS,
        @JsonProperty("move-learn-method")
        MOVE_LEARN_METHOD,
        @JsonProperty("move-target")
        MOVE_TARGET,
        @JsonProperty("nature")
        NATURE,
        @JsonProperty("pal-park-area")
        PAL_PARK_AREA,
        @JsonProperty("pokeathlon-stat")
        POKEATHLON_STAT,
        @JsonProperty("pokedex")
        POKEDEX,
        @JsonProperty("pokemon")
        POKEMON,
        @JsonProperty("pokemon-color")
        POKEMON_COLOR,
        @JsonProperty("pokemon-form")
        POKEMON_FORM,
        @JsonProperty("pokemon-habitat")
        POKEMON_HABITAT,
        @JsonProperty("pokemon-shape")
        POKEMON_SHAPE,
        @JsonProperty("pokemon-species")
        POKEMON_SPECIES,
        @JsonProperty("region")
        REGION,
        @JsonProperty("stat")
        STAT,
        @JsonProperty("super-contest-effect")
        SUPER_CONTEST_EFFECT,
        @JsonProperty("type")
        TYPE,
        @JsonProperty("version")
        VERSION,
        @JsonProperty("version-group")
        VERSION_GROUP
    }
}
