package com.anton.kop.pokemonApp.data.client.model.pokemon;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Stats {

    HP("hp", "https://pokeapi.co/api/v2/stat/1/"),
    ATTACK("attack", "https://pokeapi.co/api/v2/stat/2/"),
    DEFENSE("defense", "https://pokeapi.co/api/v2/stat/3/"),
    SPECIAL_ATTACK("special-attack", "https://pokeapi.co/api/v2/stat/4/"),
    SPECIAL_DEFENSE("special-defense", "https://pokeapi.co/api/v2/stat/5/"),
    SPEED("speed", "https://pokeapi.co/api/v2/stat/6/"),
    ACCURACY("accuracy", "https://pokeapi.co/api/v2/stat/7/"),
    EVASION("evasion", "https://pokeapi.co/api/v2/stat/8/");

    @Getter
    private String name;
    @Getter
    private String url;

    Stats(String name, String url) {
        this.name = name;
        this.url = url;
    }

    @JsonCreator
    public static Stats fromName(final JsonNode jsonNode) {
        for (Stats stat : Stats.values()) {
            if (stat.name.equals(jsonNode.get("name").asText())) {
                return stat;
            }
        }
        log.error("enum element with name {} not found", jsonNode.get("name").asText());
        return null;
    }
}
