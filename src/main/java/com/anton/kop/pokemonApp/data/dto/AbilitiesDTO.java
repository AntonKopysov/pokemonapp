package com.anton.kop.pokemonApp.data.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@NoArgsConstructor
@Data
@Table(name = "abilities")
public class AbilitiesDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer abilitiesId;
    private String name;
    private String url;
    private boolean isHidden;
    private Integer slot;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(joinColumns = @JoinColumn(name = "big_pokemons_id"),
            inverseJoinColumns = @JoinColumn(name = "abilities_id"))
    private Set<PokemonDTO> pokemon;
}
