package com.anton.kop.pokemonApp.data;

import com.anton.kop.pokemonApp.data.client.IPokemonApi;
import com.anton.kop.pokemonApp.data.client.model.Services;
import com.anton.kop.pokemonApp.data.repos.IServicesRepo;
import com.anton.kop.pokemonApp.domain.IServicesDataSource;
import com.anton.kop.pokemonApp.domain.entities.ServiceEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;

@RequiredArgsConstructor
@Slf4j
@Component
public class ServiceGateway implements IServicesDataSource {

    private final IServicesRepo servicesRepo;
    private final IPokemonApi pokemonApi;

    @Override
    public Collection<ServiceEntity> getAllServiceEntities() {
        return new ArrayList<>(servicesRepo.findAll());
    }

    private Collection<ServiceEntity> getServicesFromApi() {
        EnumMap<Services.ServicesKeys, String> map = pokemonApi.getServicesCall();
        Collection<ServiceEntity> result = new ArrayList<>();
        for (Map.Entry<Services.ServicesKeys, String> servicesKeysStringEntry : map.entrySet()) {
            ServiceEntity entity = new ServiceEntity();
            entity.setName(servicesKeysStringEntry.getKey().name());
            entity.setUrl(servicesKeysStringEntry.getValue());
            result.add(entity);
        }
        log.debug("result size:{}", result.size());
        return result;
    }

    @Override
    public void saveAllServiceEntitiesToDb() {
        //TODO servicesRepo.deleteAll() временная мера, разобраться почему в БД добавляются новые строки,
        // а не обнавляются уже имеющиеся там значения.
        servicesRepo.deleteAll();
        servicesRepo.saveAll(getServicesFromApi());
    }
}
