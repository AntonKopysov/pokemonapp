package com.anton.kop.pokemonApp.data.client.model.pokemon;

import com.anton.kop.pokemonApp.data.client.model.NameAndApiUrl;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class PokemonMove {

    private NameAndApiUrl move;
    @JsonProperty("version_group_details")
    private List<VersionGroupDetail> versionGroupDetails;
}
