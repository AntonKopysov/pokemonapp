package com.anton.kop.pokemonApp.data;

import com.anton.kop.pokemonApp.data.client.model.pokemon.Pokemon;
import com.anton.kop.pokemonApp.data.client.model.pokemon.PokemonAbility;
import com.anton.kop.pokemonApp.data.client.model.pokemon.PokemonStat;
import com.anton.kop.pokemonApp.data.dto.AbilitiesDTO;
import com.anton.kop.pokemonApp.data.dto.PokemonDTO;
import com.anton.kop.pokemonApp.data.dto.ReducedPokemonDTO;
import com.anton.kop.pokemonApp.data.dto.StatDTO;
import com.anton.kop.pokemonApp.domain.entities.AbilitiesEntity;
import com.anton.kop.pokemonApp.domain.entities.PokemonEntity;
import com.anton.kop.pokemonApp.domain.entities.ReducedPokemonEntity;
import com.anton.kop.pokemonApp.domain.entities.StatEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface PokemonMapper {

    @Mapping(source = "baseExperience", target = "experience")
    PokemonEntity pokemonToPokemonEntity(Pokemon pokemon);

    @Mapping(source = "pokemonAbility.ability.name", target = "name")
    @Mapping(source = "pokemonAbility.ability.url", target = "url")
    AbilitiesEntity pokemonAbilitiesToAbilitiesEntity(PokemonAbility pokemonAbility);

    ReducedPokemonEntity reducedPokemonDToToReducedPokemonEntity(ReducedPokemonDTO itemDTO);

    PokemonEntity pokemonDToToEntity(PokemonDTO pokemonDTO);

    @Mapping(source = "baseExperience", target = "experience")
    PokemonDTO pokemonToPokemonDTO(Pokemon pokemon);


    @Mapping(source = "pokemonAbility.ability.name", target = "name")
    @Mapping(source = "pokemonAbility.ability.url", target = "url")
    @Mapping(source = "dtos", target = "pokemon")
    @Mapping(target = "abilitiesId", ignore = true)
    AbilitiesDTO abilitiesToAbilitiesDTO(PokemonAbility pokemonAbility, Set<PokemonDTO> dtos);

    StatDTO statsToStatDTO(PokemonStat pokemonStat);

    StatEntity statDtoToStatEntity(StatDTO statDTO);

    AbilitiesEntity abilitiesDtoToAbilitiesEntity(AbilitiesDTO abilitiesDTO);
}
