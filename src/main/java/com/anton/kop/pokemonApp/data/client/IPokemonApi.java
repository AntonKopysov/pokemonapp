package com.anton.kop.pokemonApp.data.client;

import com.anton.kop.pokemonApp.data.client.model.PokemonService;
import com.anton.kop.pokemonApp.data.client.model.Services;
import com.anton.kop.pokemonApp.data.client.model.pokemon.Pokemon;

import java.util.EnumMap;

public interface IPokemonApi {

    EnumMap<Services.ServicesKeys, String> getServicesCall();

    /**
     * @param offset start offset
     * @param limit  how much pokemons we need
     */
    PokemonService getPokemonsCall(Integer offset, Integer limit);

    Pokemon getPokemonCall(Integer id);
}
