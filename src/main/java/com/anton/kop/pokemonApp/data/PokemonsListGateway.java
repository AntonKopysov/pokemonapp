package com.anton.kop.pokemonApp.data;

import com.anton.kop.pokemonApp.data.client.IPokemonApi;
import com.anton.kop.pokemonApp.data.client.model.NameAndApiUrl;
import com.anton.kop.pokemonApp.data.dto.ReducedPokemonDTO;
import com.anton.kop.pokemonApp.data.repos.IPokemonsListPagingRepo;
import com.anton.kop.pokemonApp.data.repos.IPokemonsRepo;
import com.anton.kop.pokemonApp.data.repos.PokemonByNameRepo;
import com.anton.kop.pokemonApp.domain.IPokemonsListDataSource;
import com.anton.kop.pokemonApp.domain.entities.PokemonEntity;
import com.anton.kop.pokemonApp.domain.entities.ReducedPokemonEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class PokemonsListGateway implements IPokemonsListDataSource {

    private final int API_URL_LENGTH = 34;

    private final IPokemonsRepo pokemonsRepo;
    private final IPokemonApi pokemonApi;
    private final PokemonMapper pokemonMapper;
    private final IPokemonsListPagingRepo pokemonsListPagingRepo;
    private final PokemonByNameRepo pokemonByNameRepo;
    private final PokemonGateway pokemonGateway;

    @Override
    public Collection<ReducedPokemonEntity> getAllReducedPokemonEntitys() {
        int pokemonsCount = pokemonApi.getPokemonsCall(0, 0).getCount();
        if (pokemonsRepo.count() == pokemonsCount) {
            return reducedPokemonItemsDToToEntity(pokemonsRepo.findAll());
        } else {
            pokemonsRepo.deleteAll();
            ArrayList<ReducedPokemonDTO> dtoList = Optional.of(nameAndApiUrlToDTO(pokemonApi.getPokemonsCall(0, pokemonsCount).getResults())).get();
            pokemonsRepo.saveAll(dtoList);
            return reducedPokemonItemsDToToEntity(dtoList);
        }
    }

    @Override
    public Collection<ReducedPokemonEntity> getPagedReducedPokemonEntitys(Integer pageNumber, Integer itemsPerPage) {
        return reducedPokemonItemsDToToEntity(pokemonsListPagingRepo.getPokemonsPage(PageRequest.of(pageNumber - 1, itemsPerPage)).getContent());
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public PokemonEntity getPokemonByName(String name) {
        int id = pokemonMapper.reducedPokemonDToToReducedPokemonEntity(pokemonByNameRepo.getPokemonByName(name)).getId();
        log.debug("pokemon with name {} has id = {}", name, id);
        return pokemonGateway.getPokemonEntityById(id);
    }

    private Collection<ReducedPokemonEntity> reducedPokemonItemsDToToEntity(Collection<ReducedPokemonDTO> itemDTOS) {
        return itemDTOS.stream().map(pokemonMapper::reducedPokemonDToToReducedPokemonEntity).collect(Collectors.toCollection(ArrayList::new));
    }

    private ArrayList<ReducedPokemonDTO> nameAndApiUrlToDTO(Collection<NameAndApiUrl> nameAndApiUrls) {
        return nameAndApiUrls.stream().map(nameAndApiUrl -> new ReducedPokemonDTO(pokemonUrlToId(nameAndApiUrl.getUrl()),
                nameAndApiUrl.getName(), nameAndApiUrl.getUrl())).collect(Collectors.toCollection(ArrayList::new));
    }

    private Integer pokemonUrlToId(String url) {
        return Integer.parseInt(url.substring(API_URL_LENGTH, url.length() - 1));
    }
}
