package com.anton.kop.pokemonApp.data.repos;

import com.anton.kop.pokemonApp.data.dto.ReducedPokemonDTO;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository

public class PokemonByNameRepo implements IPokemonByNameRepo {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ReducedPokemonDTO getPokemonByName(String name) {
        Query nativeQuery = initQuery(name);
        return (ReducedPokemonDTO) initQuery(name).getSingleResult();
    }

    private Query initQuery(String name) {
        Query query = entityManager.createNativeQuery("SELECT * FROM pokemons as p  where p.name = ?", ReducedPokemonDTO.class);
        query.setParameter(1, name);
        return query;
    }
}
