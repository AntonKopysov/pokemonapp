package com.anton.kop.pokemonApp.data.client.model.pokemon;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum PokemonSpritesKey {
    @JsonProperty("back_default")
    BACK_DEFAULT,
    @JsonProperty("back_female")
    BACK_FEMALE,
    @JsonProperty("back_shiny")
    BACK_SHINY,
    @JsonProperty("back_shiny_female")
    BACK_SHINY_FEMALE,
    @JsonProperty("front_default")
    FRONT_DEFAULT,
    @JsonProperty("front_female")
    FRONT_FEMALE,
    @JsonProperty("front_shiny")
    FRONT_SHINY,
    @JsonProperty("front_shiny_female")
    FRONT_SHINY_FEMALE
}
