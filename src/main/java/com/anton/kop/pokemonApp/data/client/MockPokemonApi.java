package com.anton.kop.pokemonApp.data.client;

import com.anton.kop.pokemonApp.data.client.model.NameAndApiUrl;
import com.anton.kop.pokemonApp.data.client.model.PokemonService;
import com.anton.kop.pokemonApp.data.client.model.Services;
import com.anton.kop.pokemonApp.data.client.model.pokemon.Pokemon;

import java.util.EnumMap;
import java.util.List;

public class MockPokemonApi implements IPokemonApi {

    @Override
    public EnumMap<Services.ServicesKeys, String> getServicesCall() {
        EnumMap<Services.ServicesKeys, String> result = new EnumMap<>(Services.ServicesKeys.class);
        result.put(Services.ServicesKeys.POKEMON, "pokemon");
        return result;
    }

    @Override
    public PokemonService getPokemonsCall(Integer offset, Integer limit) {
        PokemonService pokemonService = new PokemonService();
        pokemonService.setCount(1);
        pokemonService.setNext("next");
        pokemonService.setPrevious("prev");
        pokemonService.setResults(List.of(new NameAndApiUrl()));
        return pokemonService;
    }

    @Override
    public Pokemon getPokemonCall(Integer id) {
        Pokemon pokemon = new Pokemon();
        pokemon.setId(id);
        pokemon.setName("mock_name");
        pokemon.setWeight(100);
        pokemon.setHeight(200);
        pokemon.setOrder(300);
        pokemon.setDefault(true);
        return pokemon;
    }
}
