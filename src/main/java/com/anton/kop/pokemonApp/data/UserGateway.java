package com.anton.kop.pokemonApp.data;

import com.anton.kop.pokemonApp.data.dto.UserDTO;
import com.anton.kop.pokemonApp.data.repos.IUserRepo;
import com.anton.kop.pokemonApp.domain.IUserDataSource;
import com.anton.kop.pokemonApp.domain.entities.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserGateway implements IUserDataSource {

    private final UserMapper userMapper;
    private final IUserRepo userRepo;

    @Override
    public User getUserByName(String name) {
        UserDTO userDTO = userRepo.findByUsername(name);
        return userDTO == null ? null : userMapper.userDToToUser(userDTO);
    }

    @Override
    public void saveUser(User user) {
        userRepo.save(userMapper.userToUserDTO(user));
    }
}
