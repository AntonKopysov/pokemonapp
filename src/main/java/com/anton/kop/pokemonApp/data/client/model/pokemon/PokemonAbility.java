package com.anton.kop.pokemonApp.data.client.model.pokemon;

import com.anton.kop.pokemonApp.data.client.model.NameAndApiUrl;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PokemonAbility {
    private NameAndApiUrl ability;
    @JsonProperty("is_hidden")
    private boolean isHidden;
    private Integer slot;
}
