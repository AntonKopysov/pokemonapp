package com.anton.kop.pokemonApp.data.repos;

import com.anton.kop.pokemonApp.data.dto.PokemonDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPokemonRepo extends JpaRepository<PokemonDTO, Integer> {
}
