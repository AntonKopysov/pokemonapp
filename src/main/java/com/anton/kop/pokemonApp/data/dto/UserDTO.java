package com.anton.kop.pokemonApp.data.dto;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "usr")
public class UserDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String username;
    private String password;
    @Column(name = "active")
    private Boolean isActive;
}
