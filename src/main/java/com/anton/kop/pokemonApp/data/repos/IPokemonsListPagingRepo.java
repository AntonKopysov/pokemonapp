package com.anton.kop.pokemonApp.data.repos;

import com.anton.kop.pokemonApp.data.dto.ReducedPokemonDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface IPokemonsListPagingRepo extends PagingAndSortingRepository<ReducedPokemonDTO, Long> {

    @Query("select pokemon from ReducedPokemonDTO pokemon")
    Page<ReducedPokemonDTO> getPokemonsPage(Pageable pageable);
}
