package com.anton.kop.pokemonApp.data.client.model.pokemon;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PokemonStat {

    @JsonProperty("base_stat")
    private Integer baseStat;
    private Integer effort;
    private Stats stat;
}
