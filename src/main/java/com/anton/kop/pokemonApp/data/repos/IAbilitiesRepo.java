package com.anton.kop.pokemonApp.data.repos;

import com.anton.kop.pokemonApp.data.dto.AbilitiesDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAbilitiesRepo extends JpaRepository<AbilitiesDTO, Integer> {
}
