package com.anton.kop.pokemonApp.data.client.model;

import lombok.Data;

@Data
public abstract class NamedEntity {
    private String name;
}
