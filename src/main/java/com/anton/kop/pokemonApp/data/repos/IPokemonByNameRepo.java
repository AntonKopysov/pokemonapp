package com.anton.kop.pokemonApp.data.repos;

import com.anton.kop.pokemonApp.data.dto.ReducedPokemonDTO;

public interface IPokemonByNameRepo {
    ReducedPokemonDTO getPokemonByName(String name);
}
