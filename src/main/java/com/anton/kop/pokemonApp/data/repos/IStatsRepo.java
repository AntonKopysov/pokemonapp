package com.anton.kop.pokemonApp.data.repos;

import com.anton.kop.pokemonApp.data.dto.StatDTO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IStatsRepo extends JpaRepository<StatDTO, Integer> {
}
