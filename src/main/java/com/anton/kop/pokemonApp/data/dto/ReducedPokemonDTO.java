package com.anton.kop.pokemonApp.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "pokemons")
public class ReducedPokemonDTO {
    @Id
    Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "url")
    private String url;
}
