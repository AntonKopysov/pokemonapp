package com.anton.kop.pokemonApp.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "big_pokemons")
public class PokemonDTO {
    @Id
    private Integer id;
    @Column(name = "height")
    private Integer height;
    @Column(name = "weight")
    private Integer weight;
    @Column(name = "experience")
    private Integer experience;
    @Column(name = "name")
    private String name;
    @Column(name = "abilities")
    @ManyToMany(mappedBy = "pokemon", fetch = FetchType.EAGER)
    private List<AbilitiesDTO> abilities = new ArrayList<>();
    @Column(name = "stat")
    @ManyToMany(mappedBy = "pokemon", fetch = FetchType.LAZY)
    private List<StatDTO> stats = new ArrayList<>();
}
