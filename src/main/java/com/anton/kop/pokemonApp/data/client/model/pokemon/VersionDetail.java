package com.anton.kop.pokemonApp.data.client.model.pokemon;

import com.anton.kop.pokemonApp.data.client.model.NameAndApiUrl;
import lombok.Data;

@Data
public class VersionDetail {
    private Integer rarity;
    private NameAndApiUrl version;
}
