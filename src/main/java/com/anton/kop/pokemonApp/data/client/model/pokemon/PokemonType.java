package com.anton.kop.pokemonApp.data.client.model.pokemon;

import lombok.Data;

@Data
public class PokemonType {
    private Integer slot;
    private Types type;
}
