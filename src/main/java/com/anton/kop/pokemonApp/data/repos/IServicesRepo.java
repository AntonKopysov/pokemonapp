package com.anton.kop.pokemonApp.data.repos;

import com.anton.kop.pokemonApp.domain.entities.ServiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IServicesRepo extends JpaRepository<ServiceEntity, Integer> {
}
