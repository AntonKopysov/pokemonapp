package com.anton.kop.pokemonApp.data.repos;

import com.anton.kop.pokemonApp.data.dto.UserDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepo extends JpaRepository<UserDTO, Long> {
    UserDTO findByUsername(String username);
}
