package com.anton.kop.pokemonApp.data.dto;

import com.anton.kop.pokemonApp.data.client.model.pokemon.Stats;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@Table(name = "stat")
public class StatDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "base_stat")
    private Integer baseStat;
    @Column(name = "effort")
    private Integer effort;
    @Enumerated(EnumType.ORDINAL)
    private Stats stat;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(joinColumns = @JoinColumn(name = "big_pokemons_id"),
            inverseJoinColumns = @JoinColumn(name = "stat_id"))
    private Set<PokemonDTO> pokemon;
}
