package com.anton.kop.pokemonApp.data.client.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public abstract class EntityAddUrl extends NamedEntity{
    private String url;
}
