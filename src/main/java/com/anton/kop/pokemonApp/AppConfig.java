package com.anton.kop.pokemonApp;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
@EnableCaching
@EnableScheduling
@PropertySource("classpath:application.properties")
@PropertySource("classpath:db.properties")
public class AppConfig {

    @Bean
    RestTemplate getRestTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    static MockApiFactoryPostProcessor getMockApiFactoryPostProcessor(Environment environment) {
        return new MockApiFactoryPostProcessor(environment);
    }

    @Bean
    public Docket swagger() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.anton.kop.pokemonApp.presentation"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(pokeApiInfo());
    }

    private ApiInfo pokeApiInfo() {
        return new ApiInfoBuilder().title("Pokemon API")
                .description("proxy for pokeapi.co API")
                .contact(new Contact("Anton Kopysov", null, "antonhapp@gmail.com"))
                .build();
    }
}