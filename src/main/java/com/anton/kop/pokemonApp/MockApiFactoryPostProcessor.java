package com.anton.kop.pokemonApp;

import com.anton.kop.pokemonApp.data.client.MockAbleAPI;
import com.anton.kop.pokemonApp.data.client.MockPokemonApi;
import com.anton.kop.pokemonApp.data.client.PokemonApiCall;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.Objects;

/**
 * this BFPP find  beanDefinitions annotated @MockAbleAPI and if property pi.mockEnable=true change class name at
 * beanDefinition to Mock class name
 */

@AllArgsConstructor
public class MockApiFactoryPostProcessor implements BeanFactoryPostProcessor {

    private final String API_PROPERTY_NAME = "api.mockEnable";

    private Environment environment;

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        if (checkProperty()) changeBeanDefinition(configurableListableBeanFactory);
    }

    private boolean checkProperty() {
        return ((AbstractEnvironment) environment).getPropertySources().stream()
                .anyMatch(propertySource -> propertySource.containsProperty(API_PROPERTY_NAME)
                        && Boolean.parseBoolean(Objects.requireNonNull(propertySource.getProperty(API_PROPERTY_NAME)).toString()));
    }

    private void changeBeanDefinition(ConfigurableListableBeanFactory beanFactory) {
        Arrays.stream(beanFactory.getBeanDefinitionNames()).filter(s -> {
            try {
                Class<?> beanClass = null;
                if (beanFactory.getBeanDefinition(s).getBeanClassName() != null) {
                    beanClass = Class.forName(beanFactory.getBeanDefinition(s).getBeanClassName());
                }
                return beanClass != null && beanClass.getAnnotation(MockAbleAPI.class) != null && beanClass.equals(PokemonApiCall.class);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                return false;
            }
        }).forEach(s -> beanFactory.getBeanDefinition(s).setBeanClassName(MockPokemonApi.class.getName()));
    }
}