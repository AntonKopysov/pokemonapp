package com.anton.kop.pokemonApp.domain.entities;

import lombok.Data;

@Data
public class ReducedPokemonEntity extends AbstractEntityWithUrl {
    private Integer id;
}
