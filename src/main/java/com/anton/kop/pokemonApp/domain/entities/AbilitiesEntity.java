package com.anton.kop.pokemonApp.domain.entities;

import lombok.Data;

@Data
public class AbilitiesEntity extends AbstractEntityWithUrl {
    private boolean isHidden;
    private Integer slot;
}
