package com.anton.kop.pokemonApp.domain.entities;

import lombok.Data;

@Data
public class User {
    private String username;
    private String password;
    private Boolean isActive;
}
