package com.anton.kop.pokemonApp.domain.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@EqualsAndHashCode(callSuper = true)
@Data
@MappedSuperclass
public class AbstractEntityWithUrl extends BaseAbstractEntity {
    @Column
    private String url;
}
