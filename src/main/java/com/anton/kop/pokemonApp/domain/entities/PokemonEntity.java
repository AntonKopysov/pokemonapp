package com.anton.kop.pokemonApp.domain.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class PokemonEntity extends BaseAbstractEntity {

    private Integer id;
    private Integer height;
    private Integer weight;
    private Integer experience;
    private String name;
    private List<AbilitiesEntity> abilities;
    private List<StatEntity> stats;
}
