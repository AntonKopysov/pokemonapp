package com.anton.kop.pokemonApp.domain.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public abstract class BaseAbstractEntity {
    @Column
    private String name;
}
