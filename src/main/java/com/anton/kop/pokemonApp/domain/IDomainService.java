package com.anton.kop.pokemonApp.domain;

import com.anton.kop.pokemonApp.domain.entities.PokemonEntity;
import com.anton.kop.pokemonApp.domain.entities.ReducedPokemonEntity;
import com.anton.kop.pokemonApp.domain.entities.ServiceEntity;
import com.anton.kop.pokemonApp.domain.entities.User;

import java.util.Collection;

public interface IDomainService {

    Collection<ServiceEntity> getServices();

    PokemonEntity getPokemon(Integer id);

    Collection<ReducedPokemonEntity> getPokemonsList(Integer page, Integer limit);

    PokemonEntity getPokemonByName(String name);

    User getUserByName(String name);

    void saveUser(User user);
}
