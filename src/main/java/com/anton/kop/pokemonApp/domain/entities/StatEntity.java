package com.anton.kop.pokemonApp.domain.entities;

import com.anton.kop.pokemonApp.data.client.model.pokemon.Stats;
import lombok.Data;

@Data
public class StatEntity {
    private Integer baseStat;
    private Integer effort;
    private Stats stat;
}
