package com.anton.kop.pokemonApp.domain;

import com.anton.kop.pokemonApp.domain.entities.PokemonEntity;

public interface IPokemonDataSource {
    PokemonEntity getPokemonEntityById(Integer id);
}
