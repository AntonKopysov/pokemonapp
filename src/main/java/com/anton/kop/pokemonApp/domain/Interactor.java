package com.anton.kop.pokemonApp.domain;

import com.anton.kop.pokemonApp.domain.entities.PokemonEntity;
import com.anton.kop.pokemonApp.domain.entities.ReducedPokemonEntity;
import com.anton.kop.pokemonApp.domain.entities.ServiceEntity;
import com.anton.kop.pokemonApp.domain.entities.User;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Slf4j
@AllArgsConstructor
@Service
public class Interactor implements IDomainService {

    private final String POKEMON_CACHE_NAME = "pokemon";

    private IServicesDataSource servicesDataSource;
    private IPokemonsListDataSource pokemonsListDataSource;
    private IPokemonDataSource pokemonDataSource;
    private IUserDataSource userDataSource;

    @Autowired
    public void startService() {
        if (servicesDataSource != null) {
            servicesDataSource.saveAllServiceEntitiesToDb();
            pokemonsListDataSource.getAllReducedPokemonEntitys();
        }
    }

    @Override
    public Collection<ServiceEntity> getServices() {
        Collection<ServiceEntity> serviceEntities = servicesDataSource.getAllServiceEntities();
        log.debug("getServices() return {} services", serviceEntities.size());
        return serviceEntities;
    }


    @Override
    @Cacheable(value = POKEMON_CACHE_NAME, condition = "#id %2 == 0")
    public PokemonEntity getPokemon(Integer id) {
        PokemonEntity entity = pokemonDataSource.getPokemonEntityById(id);
        log.debug("getPokemon(Integer id) return pokemon with id {}", entity.getId());
        return entity;
    }

    @Scheduled(fixedDelayString = "${cacheEvictDelay.inMillis}")
    @CacheEvict(cacheNames = POKEMON_CACHE_NAME, allEntries = true)
    public void evictPokemonCache() {
        log.debug("Pokemon cache evicted");
    }

    @Override
    public Collection<ReducedPokemonEntity> getPokemonsList(Integer page, Integer limit) {
        Collection<ReducedPokemonEntity> result = new ArrayList<>(pokemonsListDataSource.getPagedReducedPokemonEntitys(page, limit));
        log.debug("getPokemonsList(Integer page, Integer limit) return {} pokemons", result.size());
        return result;
    }

    @Override
    public PokemonEntity getPokemonByName(String name) {
        PokemonEntity result = pokemonsListDataSource.getPokemonByName(name);
        log.debug("get pokemon with name: {}", result.getName());
        return result;
    }

    @Override
    public User getUserByName(String name) {
        User user = userDataSource.getUserByName(name);
        if (user != null) log.debug("get user with name: {}", user.getUsername());
        else log.debug("user is null");
        return user;
    }

    @Override
    public void saveUser(User user) {
        userDataSource.saveUser(user);
    }
}
