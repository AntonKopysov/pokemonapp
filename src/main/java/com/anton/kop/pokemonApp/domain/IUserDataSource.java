package com.anton.kop.pokemonApp.domain;

import com.anton.kop.pokemonApp.domain.entities.User;

public interface IUserDataSource {
    User getUserByName(String name);

    void saveUser(User user);
}
