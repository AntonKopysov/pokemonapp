package com.anton.kop.pokemonApp.domain;

import com.anton.kop.pokemonApp.domain.entities.PokemonEntity;
import com.anton.kop.pokemonApp.domain.entities.ReducedPokemonEntity;

import java.util.Collection;

public interface IPokemonsListDataSource {

    Collection<ReducedPokemonEntity> getAllReducedPokemonEntitys();

    Collection<ReducedPokemonEntity> getPagedReducedPokemonEntitys(Integer pageNumber, Integer itemsPerPage);

    PokemonEntity getPokemonByName(String name);
}
