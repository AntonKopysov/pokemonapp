package com.anton.kop.pokemonApp.domain;

import com.anton.kop.pokemonApp.domain.entities.ServiceEntity;

import java.util.Collection;

public interface IServicesDataSource {
    Collection<ServiceEntity> getAllServiceEntities();

    void saveAllServiceEntitiesToDb();
}
